def is_triangle(a, b, c):
    are_all_positive = (a > 0 and b > 0 and c > 0)
    is_triangle_req = (a + b > c) and (a + c > b) and (b + c > a)
    return are_all_positive and is_triangle_req
